<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gửi mail </title>
    </head>
    <body>
    <td>
        <table style="border-spacing:0;border-collapse:collapse;width:100%;margin:40px 0 20px">
            <tbody>
                <tr>
                    <td>
            <center>
                <table
                    style="border-spacing:0;border-collapse:collapse;width:560px;text-align:left;margin:0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <table style="border-spacing:0;border-collapse:collapse;width:100%">
                                    <tbody>
                                        <tr>
                                            <td>

                                                <h1
                                                    style="font-weight:normal;margin:0;font-size:30px;color:#333">
                                                    <a href="https://thevapeclub.vn"
                                                       style="font-size:30px;text-decoration:none;color:#333"
                                                       target="_blank"
                                                       data-saferedirecturl="https://www.google.com/url?q=https://thevapeclub.vn&amp;source=gmail&amp;ust=1655483413010000&amp;usg=AOvVaw3mqQmYH-B0jekACwJJY5LC">Kingsman</a>
                                                </h1>

                                            </td>
                                            <td
                                                style="text-transform:uppercase;font-size:14px;text-align:right;color:#999">
                                                <span style="font-size:16px">
                                                    Đơn hàng TVC-144564
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </center>
    </td>
</tr>
</tbody>
</table>
<table style="border-spacing:0;border-collapse:collapse;width:100%">
    <tbody>
        <tr>
            <td style="padding-bottom:40px">
    <center>
        <table
            style="border-spacing:0;border-collapse:collapse;width:560px;text-align:left;margin:0 auto">
            <tbody>
                <tr>
                    <td>
                        <h2 style="font-weight:normal;margin:0;font-size:24px;margin-bottom:10px">
                            Cám ơn bạn đã mua hàng!
                        </h2>
                        <p style="margin:0;color:#777;line-height:150%;font-size:16px">
                            Xin chào Khang, Chúng tôi đã nhận được đặt hàng của bạn và đã sẵn sàng để
                            vận chuyển. Chúng tôi sẽ thông báo cho bạn khi đơn hàng được gửi đi.
                        </p>

                        <table
                            style="border-spacing:0;border-collapse:collapse;width:100%;margin-top:20px">
                            <tbody>
                                <tr>
                                    <td>
                                        <table
                                            style="border-spacing:0;border-collapse:collapse;float:left;margin-right:15px">
                                            <tbody>
                                                <tr>
                                                    <td
                                                        style="text-align:center;padding:20px 25px;border-radius:4px; background:#1666a2">
                                                        <a href="https://thevapeclub.vn/account/orders/3ef26b22e8af44548d6198e5f323371c"
                                                           style="font-size:16px;text-decoration:none;color:#fff"
                                                           target="_blank">Xem đơn hàng</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table
                                            style="border-spacing:0;border-collapse:collapse;margin-top:19px">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <a href="https://thevapeclub.vn"
                                                           style="font-size:16px;text-decoration:none;color:#1666a2"
                                                           target="_blank"><span
                                                                style="font-size:16px;color:#999;display:inline-block;margin-right:10px">hoặc</span>
                                                            Đến cửa hàng của chúng tôi</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</td>
</tr>
</tbody>
</table>

<table style="border-spacing:0;border-collapse:collapse;width:100%;border-top:1px solid #e5e5e5">
    <tbody>
        <tr>
            <td style="padding:40px 0">
    <center>
        <table
            style="border-spacing:0;border-collapse:collapse;width:560px;text-align:left;margin:0 auto">
            <tbody>
                <tr>
                    <td>
                        <h3 style="font-weight:normal;margin:0;font-size:20px;margin-bottom:25px">
                            Thông tin khách hàng
                        </h3>
                    </td>
                </tr>
            </tbody>
        </table>
        <table
            style="border-spacing:0;border-collapse:collapse;width:560px;text-align:left;margin:0 auto">
            <tbody>
                <tr>
                    <td>
                        <table style="border-spacing:0;border-collapse:collapse;width:100%">
                            <tbody>
                                <tr>
                                    <td style="padding-bottom:40px;width:50%">
                                        <h4
                                            style="font-weight:500;margin:0;font-size:16px;color:#555;margin-bottom:5px">
                                            Địa chỉ giao hàng
                                        </h4>
                                        <p style="margin:0;color:#777;line-height:150%;font-size:16px">
                                            Đỗ Trung Khang<br>
                                            Đối diện trường THPT Hai Bà Trưng Thạch Thất, xã Tân Xã
                                            <br>Huyện
                                            Thạch Thất, Hà Nội
                                        </p>
                                    </td>


                                    <td style="padding-bottom:40px;width:50%">
                                        <h4
                                            style="font-weight:500;margin:0;font-size:16px;color:#555;margin-bottom:5px">
                                            Địa chỉ thanh toán
                                        </h4>
                                        <p style="margin:0;color:#777;line-height:150%;font-size:16px">
                                            Đỗ Trung Khang<br>
                                            Đối diện trường THPT Hai Bà Trưng Thạch Thất, xã Tân Xã
                                            <br>Huyện Thạch Thất, Hà Nội
                                        </p>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                        <table style="border-spacing:0;border-collapse:collapse;width:100%">
                            <tbody>
                                <tr>
                                    <td style="padding-bottom:40px;width:50%">
                                        <h4
                                            style="font-weight:500;margin:0;font-size:16px;color:#555;margin-bottom:5px">
                                            Phương
                                            thức
                                            vận
                                            chuyển
                                        </h4>

                                        <p style="margin:0;color:#777;line-height:150%;font-size:16px">
                                            Ship
                                            trong
                                            48
                                            tiếng
                                        </p>

                                    </td>
                                    <td style="padding-bottom:40px;width:50%">
                                        <h4
                                            style="font-weight:500;margin:0;font-size:16px;color:#555;margin-bottom:5px">
                                            Phương
                                            thức
                                            thanh
                                            toán
                                        </h4>
                                        <p style="margin:0;color:#777;line-height:150%;font-size:16px">
                                            Thanh
                                            toán
                                            khi
                                            giao
                                            hàng
                                            (COD)
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</td>
</tr>
</tbody>
</table>
<table style="border-spacing:0;border-collapse:collapse;width:100%;border-top:1px solid #e5e5e5">
    <tbody>
        <tr>
            <td style="padding:35px 0">
    <center>
        <table
            style="border-spacing:0;border-collapse:collapse;width:560px;text-align:left;margin:0 auto">
            <tbody>
                <tr>
                    <td style="">
                        <p style="margin:0;color:#999;line-height:150%;font-size:14px">
                            Nếu
                            bạn
                            có
                            bất
                            cứ
                            câu
                            hỏi
                            nào,
                            đừng
                            ngần
                            ngại
                            liên
                            lạc
                            với
                            chúng
                            tôi
                            tại
                            <a href="mailto:thevapeclub@helix.com.vn"
                               style="font-size:14px;text-decoration:none;color:#1666a2"
                               target="_blank">thevapeclub@helix.com.vn</a>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</td>
</tr>
</tbody>
</table>
<img src="https://ci4.googleusercontent.com/proxy/AkPYSwbfCTPpa9UY2iemTt-8uuNCxd9wMi-MxiDXCwCclRn4IrvavPQy53Rok8pDmYePvpYw7glbcjctupZqDJjD9WVBMoR1vQ=s0-d-e1-ft#http://hstatic.net/0/0/global/notifications/spacer.png"
     height="0" style="min-width:600px;height:0" class="CToWUd">
</body>
</html>

