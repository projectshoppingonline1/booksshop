
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Danh sách đơn hàng</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="./assets/css/styles.css">
        <link rel="stylesheet" href="./assets/css/style.css">
        <link rel="stylesheet" href="./assets/fonts/themify-icons/themify-icons.css">
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="./assets/css/style.css">
        <link rel="stylesheet" href="./assets/fonts/themify-icons/themify-icons.css">
        <%@include file="components/javascript.jsp" %>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

        <script src="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"></script>
        <style>

            .payment-method__item-name {
                font-size: 20px;
                padding-left: 20px;
            }

            .payment-method__item {
                display: flex;
                align-items: center;
                border: 1px solid #D9D9D9;
                border-radius: 16px;
                padding: 15px 20px;
                margin-bottom: 1rem;
            }



            .payment-method__item-icon-wrapper img {
                min-width: 100px;
                max-height: 100px;
                max-width: 100px;
                padding-left: 40px;
                image-rendering: -webkit-optimize-contrast;
            }
            .body_cartCompletion {
                font-family: sans-serif;
                background: linear-gradient(110deg, #fdfdbe 60%, #f9f86c 60%);
            }
            .groundy{
                font-family: sans-serif;
                background: linear-gradient(110deg, #fdfdbe 60%, #f9f86c 60%);
            }

            .circle {
                height: 10px;
                width: 10px;
                border: 50%;
            }
            .mtop {
                margin-top: 2%;
            }
            .title-order {
                display: flex;
                justify-content: center;
                color: red;
            }
            .tbborder {
                border: 2px solid black;
            }
            .sb-nav-fixed #layoutSidenav #layoutSidenav_content{
                justify-content: center;
            }
            #layoutSidenav #layoutSidenav_content{
                justify-content: flex-start;
            }


        </style>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
    </head>
    <body>
        <%@include file="components/account.jsp" %>
        <%@include file="components/manager-header.jsp" %>

        <div id="layoutSidenav">
            <%@include file="components/sale-left-dashboard.jsp" %>
            <div class="groundy" id="layoutSidenav_content">

                <h2 class="mtop title-order" >Danh sách các đơn hàng</h2>
                <div class="container mtop" style="width:100%">
                    <div class="row-lg-2">
                        <label for="recordLength">Hiển thị <input type="number" id="recordLength" min="1" max="100" value="10" style="width: 55px"> bản ghi | </label>

                        <label for="startDate">Từ ngày:</label>
                        <input type="date" id="startDate">
                        <label for="endDate">Đến ngày:</label>
                        <input type="date" id="endDate">
                        <label for="statusFilter"> | Tình trạng:</label>
                        <select id="statusFilter" style="width: 150px;">
                            <option value="">Tất cả</option>
                            <option value="Chờ xác nhận">Chờ xác nhận</option>
                            <option value="Xác nhận">Xác nhận</option>
                            <option value="Đã thanh toán và chờ xác nhận">Đã thanh toán và chờ xác nhận</option>
                            <option value="Thành công">Thành công</option>
                            <option value="Đã hủy">Đã hủy</option>
                        </select><c:if test = "${sessionScope.us.role_Id == 4}">
                        <label for="statusFilter"> | Nhân Viên Sale:</label>
                        <select id="salerFilter1"style="width: 100px;">
                            <option value="">Tất cả</option>
                            <c:forEach items="${salerList}" var="saler">
                                <option value="${saler}">${saler}</option>
                            </c:forEach>
                        </select><br></c:if>
                        <button id="applyBtn">Áp dụng</button>
                    </div>

                    <table class="table table-striped table-bordered tbborder" id="sortTable">
                        <thead>
                            <tr>
                                <th>OrderID</th>
                                <th>Ngày&nbspmua&nbsphàng</th>
                                <th>Sản&nbspphẩm</th>
                                <th>Tổng&nbspchi&nbspphí</th>
                                <th>Tên&nbspkhách&nbsphàng</th>
                                <th>Nhân&nbspviên&nbspsale</th>
                                <th>Tình&nbsptrạng</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test = "${sessionScope.us.role_Id == 3}">
                                <c:forEach items ="${OrderListsaler}" var="d">
                                    <tr>
                                        <td><a href="order-detail-sale?orderId=${d.orderID}">
                                                ${d.orderID}</a></td>
                                        <td>${d.date}</td>
                                        <c:if test="${d.countProduct != 0}">
                                            <td>${d.fullNameFirstProduct} và ${d.countProduct} sản phẩm khác</td>
                                        </c:if>
                                        <c:if test="${d.countProduct == 0}">
                                            <td>${d.fullNameFirstProduct}</td>
                                        </c:if>
                                        <td>${d.total_cost}</td>
                                        <td>${d.fullName}</td>
                                        <td>${d.fullNameSaler}</td>

                                        <td>
                                            ${d.status_order_name}
                                        </td>

                                    </tr>

                                </c:forEach>
                            </c:if>
                            <c:if test = "${sessionScope.us.role_Id == 4}">
                                <c:forEach items ="${OrderList}" var="c">
                                    <tr>
                                        <td><a href="order-detail-sale?orderId=${c.orderID}">
                                                ${c.orderID}</a></td>
                                        <td>${c.date}</td>
                                        <c:if test="${c.countProduct != 0}">
                                            <td>${c.fullNameFirstProduct} và ${c.countProduct} sản phẩm khác</td>
                                        </c:if>
                                        <c:if test="${c.countProduct == 0}">
                                            <td>${c.fullNameFirstProduct}</td>
                                        </c:if>
                                        <td>${c.total_cost}</td>
                                        <td>${c.fullName}</td>
                                        <td>${c.fullNameSaler}</td>

                                        <td>
                                            ${c.status_order_name}
                                        </td>

                                    </tr>

                                </c:forEach>
                            </c:if>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function () {
                var table = $('#sortTable').DataTable({
                    "language": {
                        "decimal": "",
                        "emptyTable": "Không có đơn hàng nào",
                        "info": " _START_ đến _END_ của _TOTAL_ bản ghi",
                        "infoEmpty": "HIển thị 0 to 0 of 0 bản ghi",
                        "infoFiltered": "(kết quả từ _MAX_ tổng số bản ghi)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "",
                        "loadingRecords": "Loading...",
                        "processing": "",
                        "search": "Tìm kiếm:",
                        "zeroRecords": "Không tìm thấy kết quả nào",
                        "paginate": {
                            "first": "F",
                            "last": "L",
                            "next": "Sau",
                            "previous": "Trước"
                        },
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }
                });

                $('#applyBtn').on('click', function () {
                    var recordLength = parseInt($('#recordLength').val());
                    table.page.len(recordLength).draw();
                });
                $('#applyBtn').on('click', function () {
                    var recordLength = parseInt($('#recordLength').val());
                    table.page.len(recordLength).draw();

                    var startDate = new Date($('#startDate').val());
                    var endDate = new Date($('#endDate').val());

                    // Mảng chứa các ngày trong khoảng thời gian
                    var datesInRange = [];

                    // Thêm các ngày từ startDate đến endDate vào mảng datesInRange
                    var currentDate = new Date(startDate);
                    while (currentDate <= endDate) {
                        datesInRange.push(new Date(currentDate));
                        currentDate.setDate(currentDate.getDate() + 1);
                    }

                    // Lọc cột thứ hai (index 1) với các ngày trong khoảng thời gian
                    table
                            .columns(1)
                            .search(datesInRange.map(date => date.toISOString().slice(0, 10)).join('|'), true, false)
                            .draw();
                    var salerFilter1 = $('#salerFilter1').val();
                    table
                            .columns(5)
                            .search(salerFilter1)
                            .draw();

                    var statusFilter = $('#statusFilter').val();
                    table
                            .columns(6)
                            .search(statusFilter)
                            .draw();
                });
            });

        </script>

    </body>
</html>

