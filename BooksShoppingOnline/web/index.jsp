<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Trang chủ</title>
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css">
        <%@include file="components/javascript.jsp" %>
        <style>
            .product-price{
                text-align: left;
            }
           
            .product-price{
                text-align: left;
            }
            .product-item {
                position: relative;
                overflow: hidden;
                border-radius: 8px;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                transition: transform 0.2s ease, box-shadow 0.2s ease;
            }

            .product-item:hover {
                box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2); /* Hiệu ứng đổ bóng khi hover */
                transform: translateY(-4px); /* Hiệu ứng nhấp nháy khi hover */
            }

            .product-thumb img {
                transition: transform 0.2s ease;
            }

            .product-item:hover .product-thumb img {
                transform: scale(1.05); /* Phóng to ảnh khi hover */
            }

            .product-top {
                position: relative;
            }

            .buy-now {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                padding: 8px;
                background-color: #3498db;
                color: #fff;
                text-align: center;
                text-decoration: none;
                border-bottom-left-radius: 8px;
                border-bottom-right-radius: 8px;
                transition: background-color 0.2s ease;
            }

            .product-item:hover .buy-now {
                background-color: #2980b9; /* Màu nền khi hover */
            }
            .content-Category-section {
                padding: 60px 0;
            }

            .section-heading {
                font-size: 32px;
                font-weight: bold;
                text-align: center;
                margin-bottom: 40px;
                color: #333; /* Màu chữ */
                text-transform: uppercase; /* Chuyển thành chữ in hoa */
                border-bottom: 2px solid #3498db; /* Đường viền dưới */
                padding-bottom: 10px; /* Khoảng cách dưới */
            }

            /* Phần "Top 4 quyển sách đa dạng" */
            .background-product {
                padding: 60px 0;
            }

            .section-heading.product-heading {
                font-size: 32px;
                font-weight: bold;
                text-align: center;
                margin-bottom: 40px;
                color: #333; /* Màu chữ */
                text-transform: uppercase; /* Chuyển thành chữ in hoa */
                border-bottom: 2px solid #3498db; /* Đường viền dưới */
                padding-bottom: 10px; /* Khoảng cách dưới */
            }

            .product-down {
                display: flex;
                justify-content: center;
            }
        
        </style>
    </head>
    <body>
        <div id="main">
            ${errorMessage}
            <%@include file="components/header.jsp" %>
            <%@include file="components/account.jsp" %>
            <!--Slider-->

            <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" >
                    <div class="carousel-indicators">

                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <c:forEach begin="1" end="${sessionScope.totalSlider-1}" var="c">    
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="${c}" aria-label="Slide ${c+1}"></button>   
                        </c:forEach>
                    </div>

                    <div class="carousel-inner">

                        <c:forEach items="${sessionScope.listSlider_HomePageAll}" var="s" >
                            <div class="carousel-item ${s.id == sessionScope.sliderFirst.id ? "active" : ""}">
                                <a href="${s.backlink}"><img src="${s.slider_image}" class="d-block w-100" alt="..."></a>

                            </div> 
                        </c:forEach>

                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>



            <!-- Content -->
            <div id="content" style="background-color: #e6efe9">
                <div class="content-section background-product" style="width: 1500px; background-color: #e6efe9">


                    <!-- Base Product -->
                    <h2 class="section-heading">Top 4 quyển sách đa dạng</h2>
                    <br/><br/><br/>
                    <div  class="container-fluid">
                        <div class="row">
                            <c:forEach items="${sessionScope.list4product}" var="p">
                                <div class="col-md-2 product-down">
                                    <div class="row">
                                        <div class="product-item">
                                            <div class="product-top">
                                                <div class="reviews-rating">

                                                    <c:forEach var="i" begin="0" end="4">
                                                        <c:if test="${(p.rated_star - i) >= 1}">
                                                            <div class="reviews-rating__star is-active"></div> 
                                                        </c:if>
                                                        <c:if test="${(p.rated_star - i) < 1 && (p.rated_star - i) > 0}">
                                                            <div class="reviews-rating__star is-active is-half"></div> 
                                                        </c:if>
                                                        <c:if test="${(p.rated_star - i) <= 0}">
                                                            <div class="reviews-rating__star"></div> 
                                                        </c:if>

                                                    </c:forEach>

                                                </div>
                                                <a href="" class="product-thumb">
                                                    <a href="list-detail?productId=${p.id}&categoryId=${p.category_id}">
                                                        <img src="${p.image}" height="365px" width="230px" alt="">
                                                    </a>
                                                </a>

                                                <c:if test="${sessionScope.us == null}" >
                                                    <a class="buy-now" data-toggle="modal"  data-target="#loginModal" style="color: white">Mua ngay</a>
                                                </c:if>
                                                <c:if test="${sessionScope.us != null}" >
                                                    <a href="addcart?productId=${p.id}" class="buy-now" >Mua ngay</a>
                                                </c:if>
                                            </div>
                                            <div class="product-infor">
                                                <diV style="width: 226px; height: 90px">
                                                    <a href="" class="product-name">${p.name}</a> 
                                                </div>
                                                <div class="product-price">
                                                    <c:if test="${p.sale_price != 0}">
                                                        ${p.sale_price}đ
                                                        <del>${p.original_price}đ</del>
                                                    </c:if>
                                                    <c:if test="${p.sale_price == 0}">
                                                        ${p.original_price}đ
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>


                <div class="content-Category-section background-product" style="background-color: #e6efe9"><!--

                    <!-- Product list -->
                    <h2 class="section-heading">Những thể loại hấp đẫn</h2>
                    <div class="category-list">
                        <a href="list?&categoryId=2"><div class="category-item category-img" style="background: url('assets/img/categoryImg/1.png')
                                                          top center / cover no-repeat">
                                <div class="category-content">Kĩ năng sống</div>
                            </div></a>

                        <a href="list?&categoryId=4"><div class="category-item category-img" style="background: url('assets/img/categoryImg/9.png')
                                                          top center / cover no-repeat">
                                <div class="category-content">Nghệ thuật - Giải trí</div>
                            </div></a>

                        <a href="list?&categoryId=1"><div class="category-item category-img" style="background: url('assets/img/categoryImg/19.png')
                                                          top center / cover no-repeat">
                                <div class="category-content">Light-Novel</div>
                            </div></a>


                        <a href="list?&categoryId=3"><div class="category-item category-img" style="background: url('assets/img/categoryImg/32.png')
                                                          top center / cover no-repeat">
                                <div class="category-content">Manga-Comic</div>
                            </div></a>

                        <div class="clear"></div>
                    </div> 
                </div>
                <!-- Blog -->
                <div class="blog-section">
                    <div class="content-section">s
                        <h2 class="section-heading text-white">Chúng tôt sẽ mang đến những tri thức cho người đọc</h2>


                        <!-- blog list hot -->
                        <div class="blog-list">

                            <c:forEach items="${sessionScope.listBlog_HomePage}" var="c">
                                <div class="blog-item">
                                    <img src="${c.thumbnail}" alt="" class="blog-img" style="width: 592px; height: 450px">
                                    <div class="blog-body" style="min-height: 340px">
                                        <h3 class="blog-heading" style="font-size: 40px">${c.title}</h3>
                                        <p class="blog-desc">${c.brief_infor}</p>
                                        <a href="blogDetail?blog_id=${c.blog_id}&categoryBlog_id=${c.categoryBlog_id}" class="place-buy-btn">Tìm hiểu thêm</a>
                                    </div>
                                </div>
                            </c:forEach>


                            <div class="clear"></div>
                        </div> 
                    </div>

                </div>
                <%@include file="components/footer.jsp" %>
                </body>
                </html>
