
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="footer">

    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7448.27256631781!2d105.53745830000003!3d21.027232300000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1653919437201!5m2!1svi!2s" 
                width=90% height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>

    <div class="info-footer">
        <div class="content-footer">
            <h2>Địa chỉ liên hệ</h2>
            <p class="mt-40">HUB Hà Nội: Hoalaclink03, thôn Vân Lôi, xã Bình Yên, huyện Thạch Thất, TP. Hà Nội</p>
        </div>

        <div class="feedback-footer">
            <h2 class="mt-40">KingsBooks lắng nghe bạn!</h2>
            <p>Chúng tôi luôn trân trọng và mong đợi nhận được mọi ý kiến đóng góp từ khách hàng để có thể nâng cấp trải nghiệm dịch vụ và sản phẩm tốt hơn nữa</p>
            <br>
            <div class="contact-info">
                
                <p><i class="ti-location-pin"></i>Tran Duy Hung, Ha Noi</p>
                <p><i class="ti-mobile"></i>Phone:+0356111214</p>
                <p><i class="ti-email"></i>dotunglam@gmail.com</p>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>


</div>
