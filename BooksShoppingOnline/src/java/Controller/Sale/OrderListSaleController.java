/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Sale;

import dal.OrderDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Order;
import model.User;

/**
 *
 * @author MSI Bravo
 */
@WebServlet(name = "OrderListSaleController", urlPatterns = {"/order-list-sale"})

public class OrderListSaleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       // String saler_Id = request.getParameter("saler_Id");
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("us");
        //saler_Id = u.getUser_Id();
        List<Order> OrderList = new OrderDao().getAllOrder();
        List<Order> OrderListsaler = new OrderDao().getAllOrdersaler(u.getUser_Id());
        List<String> salerList = new ArrayList<>();
        // Lặp qua danh sách đơn hàng và lấy tên người bán (saler) từ mỗi đơn hàng
        // Nếu tên người bán chưa tồn tại trong danh sách salerList, thêm nó vào danh sách
        for (Order order : OrderList) {
            String saler = order.getFullNameSaler();
            if (!salerList.contains(saler)) {
                salerList.add(saler);
            }
        }
        // Đặt OrderList và salerList làm thuộc tính của yêu cầu
        request.setAttribute("OrderList", OrderList);
        request.setAttribute("OrderListsaler", OrderListsaler);
        request.setAttribute("salerList", salerList);
        request.getRequestDispatcher("SaleOrderList.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
